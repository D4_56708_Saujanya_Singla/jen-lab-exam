const express = require('express')
const movieRoute = require('./routes/movie')

const app = express()

app.use(express.json())
app.use('/movies',movieRoute)


app.listen(4444,'0.0.0.0',()=>{
    console.log('server started on port 4444')
})