const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.get('/display:name',(request,response)=>{
    const name=request.params
    const statement = `select * from movie where movie_title = '${name}'`
    db.execute(statement,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})


router.post('/add',(request,response)=>{
    // movie_id, movie_title, movie_release_date,movie_time,director_name)
    const{id, title, date, time, dname} = request.body
    const statement = `insert into movie values('${id}','${title}','${date}','${time}','${dname}')`
    db.execute(statement,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.put('/edit',(request,response)=>{
    const { date , time } = request.body
    const statement = `update movie set movie_release_date = '${date}' and movie_time= '${time}'`
    db.execute(statement,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})


router.delete('/delete:name',(request,response)=>{
    const name=request.params
    const statement = `delete from movie where movie_title = '${name}'`
    db.execute(statement,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

module.exports = router