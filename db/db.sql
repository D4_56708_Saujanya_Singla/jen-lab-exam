-- movie_id, movie_title, movie_release_date,movie_time,director_name)
create table movie(
    movie_id int primary key, 
    movie_title varchar(50), 
    movie_release_date date, 
    movie_time varchar(20),
    director_name varchar(20)
    );


insert into movie values(1,'abc','2021-05-11','evening','xyz');